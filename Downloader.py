#!/usr/bin/env python

# EXTERNAL DEPS
from clint.textui import progress # python-clint

# INTERNAL DEPS
import requests

class Downloader:
	@staticmethod
	def download(url, path):
		r = requests.get(url, stream=True)
		with open(path, 'wb') as f:
			if r.headers.get('content-length') == None:
				print("Content length zero-(http error)")
				return False
			total_length = int(r.headers.get('content-length'))
			for chunk in progress.bar(r.iter_content(chunk_size=1024), expected_size=(total_length/1024) + 1): 
				if chunk:
					f.write(chunk)
					f.flush()

		return True

#!/usr/bin/env python

import argparse
import json
import sys
import os.path
import shutil
import subprocess

import requests

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By  
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException

from ThreadedDownloader import ThreadedDownloader

def merge_binaries(outname):
	files = ['.intermediates/' + x for x in os.listdir(os.path.dirname(outname)) if '.ts' in x]
	files.sort()

	with open(outname, "ab") as handle:
		for item in files:
			with open(item, "rb") as infile:
				contents = infile.read()
				handle.write(contents)

def parse_stream_list(slist):
	path_list = list()
	largest = 0
	largest_i = 0
	for item in slist.decode('utf-8').split('\n'):
		if 'm3u8' in item:
			path_list.append(item)

	for index, item in enumerate(path_list):
		head,tail = os.path.split(item)
		try:
			num = int(tail.split('.')[0])
			if num > largest:
				largest = num
				largest_i = index
		except ValueError:
			print("Bad line: " + item)

	return path_list[largest_i]

def download_from_url(url, name, driver, debug=False):
	print("Downloading " + name + "\n" + url)
	driver.get(url)

	# Wait until we find mcloud iframe
	elem = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//iframe[contains(@src, "mcloud")]')))
	print(elem.get_attribute("src"))


	if elem:
		print("Found frame")

		driver.switch_to.frame(elem)
		print("waiting..")
		
		# Wait for rest of page to load
		try:
			elem = WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, '//div[contains(@id, "player")]')))
		except TimeoutException:
			print("timed out")

		# Get HTML through JS so we can get inline js vars
		url = None
		found = False
		while not found:
			print("searching for mediaSources")
			html = driver.execute_script("return document.getElementsByTagName('html')[0].innerHTML")
			print("html")
			print(html)
			for item in html.split('\n'):
				if 'mediaSources' in item:
					found = True
					print("FOUND ITEM")
					print(item[19:-1])
					url = json.loads(item[19:-1])[0]['file']
					break

		# Search for title
		for item in html.split('\n'):
			if '<title>' in item:
				print("TITLE: " + item[7:-8])
				break

		# Copy cookies to requests format
		cookies = driver.get_cookies()
		cookies_format = dict()
		for cookie in cookies:
			cookies_format[cookie['name']] = cookie['value']

		if url:
			print("url: " + url)
		else:
			print("no url")
			sys.exit(1)

		base_url = os.path.dirname(url)

		# Set required headers
		headers = dict()
		headers['authority'] = "bxolz.mcloud.to"
		headers['path'] = "/11/4d933e7fb5b10972/4f2126a396882a6c4604b255193c187dd244924d66ded2b7a31a6af814be1604bfefb72206f2e5c6c585285504c35d099ae2fbb67493cfb05c54fa3bda305a169f10b088e26f1c267e33075078e8349ec6606d5584d3cdd85e812af9f3ecd645/list.m3u8"
		headers['referer'] = "https://mcloud.to/embed/vqq042?key=156fea8b26c6146a332be42fd7387411&autostart=true"

		# Get available formats
		resp = requests.get(url, cookies=cookies_format, headers=headers)

		best_stream = parse_stream_list(resp.content)

		full_stream_url = base_url + "/" + best_stream
		print("STREAM URL: " + full_stream_url)
		resp2 = requests.get(full_stream_url, cookies=cookies_format, headers=headers)

		ts_list = list()
		count = 1
		for item in resp2.content.decode('utf-8').split('\n'):
			if '.ts' in item:
				dict_item = dict()
				ts_url = os.path.dirname(full_stream_url) + "/" + item
				ts_name = "{0:03d}".format(count) + ".ts"
				dict_item['name'] = ".intermediates/" + ts_name
				dict_item['url'] = ts_url
				ts_list.append(dict_item)
				count = count + 1

		print("TS COUNT" + str(len(ts_list)))

		# Manage temp dir
		if os.path.exists(".intermediates"):
			shutil.rmtree(".intermediates")

		os.mkdir(".intermediates")

		print("START THREADED DOWNLOAD")
		td = ThreadedDownloader(ts_list, debug=debug,cookies=cookies_format, headers=headers)
		td.run()
		merge_binaries(".intermediates/all.ts")

		print("Converting with FFMPEG")
		subprocess.run(['ffmpeg', '-i', '.intermediates/all.ts', '-map', '0', '-c', 'copy', name + '.mp4'])
		
		print(name + " completed.")
	else:
		print("Timed out no frame")

# BEGIN EXECUTION

parser = argparse.ArgumentParser()
parser.add_argument("file", help="end file name with $ wildcard")
parser.add_argument("link", help="initial page to scrape")
parser.add_argument("-s", "--start", type=int, help="episode to start on")
parser.add_argument("-d", "--debug", action="store_true", help="display debug info")

args = parser.parse_args()

if not '$' in args.file:
	print("file argument requires a '$' wildcard")
	sys.exit(1)

driver = webdriver.Firefox()
print("test")

driver.get(args.link)

link_containers = driver.find_elements_by_class_name("server")
print(str(len(link_containers)))

found = None
for item in link_containers:
	if not 'hidden' in item.get_attribute('class').split():
		found = item
		break

if not found:
	print("No mycloud episode lists found")
	sys.exit(1)

link_elements = found.find_elements_by_tag_name("a")

if len(link_elements) > 0:
	links = [x.get_attribute("href") for x in link_elements]
	for index,item in enumerate(links):
		if args.start and args.start > (index+1):
			continue
		download_from_url(item, args.file.replace("$", str(index+1)), driver, args.debug)
else:
	print("No episode elements found in mycloud list")

driver.close()
print("Done!")

#!/usr/bin/env python

import requests
import sys
import curses
import time
from threading import Thread

class Thread_Download(Thread):
	download = None
	percent = 0
	log = ""
	cookies = None
	headers = None
	def __init__(self, download, num, cookies=None, headers=None):
		Thread.__init__(self)
		self.download = download
		self.cookies = cookies
		self.headers = headers

	def run(self):
		with open(self.download['name'], "wb") as handle:
			headers = {
				'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',
				'referer': 'https://www2.9anime.to'
			}
			response = None
			for i in range(0, 4):
				if self.headers:
					response = requests.get(self.download['url'], headers=self.headers, cookies=self.cookies, stream=True)
				else:
					response = requests.get(self.download['url'], headers=headers, stream=True)
				if response.status_code != 503 and response.status_code != 403:
					break
				time.sleep(3)

			length = response.headers.get('content-length')
			

			# DEBUG
			self.log = self.log + "Name: " + self.download['name'] + "\nResponse: " + str(response.status_code) + "\nContent-Length: " + str(length) + "\n###\n"

			if length == None or length == 0:
				return

			dl_prog = 0
			length = int(length)
			for data in response.iter_content(chunk_size=4096):
				dl_prog = dl_prog + len(data)
				handle.write(data)
				#sys.stdout.write(self.download['name'] + ".. " + str((dl_prog / length) * 100) + "%\n")
				self.percent = (dl_prog / length) * 100

class ThreadedDownloader:
	downloads = None
	threads = list()
	queue = dict()
	stdscr = None
	debug = False
	cookies = None
	headers = None
	def __init__(self, downloads, debug=False, cookies=None, headers=None):
		self.downloads = downloads
		self.debug = debug
		self.headers = headers
		self.cookies = cookies

	def __del__(self):
		if not self.debug and self.stdscr != None:
			curses.nocbreak()
			self.stdscr.keypad(False)
			curses.echo()
			curses.endwin()

	def progress_format(self, progress, size, prog_range):
		factor = int(prog_range/size)
		normalized = int(progress/factor)
		space = size - normalized
		cur_progress = "[%s%s] %s" % ("=" * normalized, " " * space, str("{0:.2f}".format((progress/prog_range)*100)) + "%")
		return cur_progress

	def add_queue(self, name, percentage):
		self.queue[name] = percentage
	
	def update_queue(self, name, percentage):
		if name in self.queue.keys():
			self.queue[name] = percentage
			
	
	def remove_queue(self, name):
		if name in self.queue.keys():
			del self.queue[name]

	def run(self):
		if not self.debug:
			self.stdscr = curses.initscr()
			curses.noecho()
			curses.cbreak()
			self.stdscr.keypad(True)

		for index, item in enumerate(self.downloads):
			toAdd = Thread_Download(item, index+1, self.cookies, self.headers)
			toAdd.setName(item['name'])
			self.threads.append(toAdd)
			toAdd.start()

		oneIsAlive = True
		while oneIsAlive:
			oneIsAlive = False	

			if not self.debug:
				height, width = self.stdscr.getmaxyx()
				self.stdscr.clear()
				for i,t in enumerate(self.threads):
					if t.is_alive():
						oneIsAlive = True

					if not self.debug:
						if len(self.queue) < height and t.percent != 100:
							self.add_queue(t.getName(), self.progress_format(t.percent, 20, 100))
							#self.add_queue(t.getName(), "{0:.2f}".format(t.percent))
						elif t.percent == 100:
							self.remove_queue(t.getName())
						else:
							self.update_queue(t.getName(), self.progress_format(t.percent, 20, 100))
							#self.update_queue(t.getName(), "{0:.2f}".format(t.percent))
				
			if not self.debug:
				count = 0
				for key,value in self.queue.items():
					self.stdscr.addstr(count, 0, key + " : " + value)
					self.stdscr.refresh()
					count = count + 1
				

			time.sleep(0.1)
		
		if not self.debug:
			curses.nocbreak()
			self.stdscr.keypad(False)
			curses.echo()
			curses.endwin()
		print(self.threads[0].log)

#!/bin/env python

# watchanime.info ripper

from selenium import webdriver
from selenium.webdriver.common.by import By 
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException, NoSuchElementException
from urllib.request import Request, urlopen
from urllib import error
from bs4 import BeautifulSoup as BS

# LOCAL
from Downloader import Downloader 
import requests
import sys 
import subprocess
import argparse
import json
import os.path

# LOAD WEB PAGES IN TRY/CATCH
def safeLoadURL(driver, link):
	try:
		print("Loading: " + link)
		driver.get(link)
		print("Page loaded..")
		return True
	except TimeoutException:
		print("Page timed out, attempting anyway..")
		return False


def newRapidVideoFetch(driver, link, quality=0):
	#LOAD VIDEO PAGE
	safeLoadURL(driver, link)

	# LOAD RAPID IFRAME
	try:
		frame = driver.find_element_by_xpath('//div[contains(@class, "rapidvideo")]/iframe')
	except NoSuchElementException:
		return False

	print("LOADING VIDEO IFRAME")
	driver.switch_to.frame(frame)
	#safeLoadURL(driver, frame.get_attribute("src"))
	
	videoElement = None
	while not videoElement: # REEEEEEEEE
		try:
			videoElement = driver.find_element_by_xpath("//video")
		except NoSuchElementException:
			print("ATTEMPTING STREAMANGO FETCH")
			frame = driver.find_element_by_xpath('//iframe[contains(@id, "embedvideo")]')	
			driver.switch_to.frame(frame)
			print("Switched to frame")
			#playBtn = driver.find_element_by_xpath('//button[contains(@class, "vjs-big-play-button")]')
			playBtn = driver.find_element_by_xpath('//div[contains(@id, "videooverlay")]')
			playBtn.click()

	pureURL = videoElement.get_attribute("src")
	print("LOCATED PURE URL: " + pureURL)
	return pureURL

def streamangoFetch(driver, item):
	safeLoadURL(driver, item)

	videoElement = None
	while not videoElement: # REEEEEEEEE
		try:
			videoElement = driver.find_element_by_xpath("//video")
		except NoSuchElementException:
			print("ATTEMPTING STREAMANGO FETCH")
			frame = driver.find_element_by_xpath('//div[contains(@class, "streamango")]/iframe')	
			driver.switch_to.frame(frame)
			print("Switched to frame")
			#playBtn = driver.find_element_by_xpath('//button[contains(@class, "vjs-big-play-button")]')
			playBtn = driver.find_element_by_xpath('//div[contains(@id, "videooverlay")]')
			playBtn.click()

	pureURL = videoElement.get_attribute("src")
	print("LOCATED PURE URL: " + pureURL)
	return pureURL



# CODE TO GET VIDEO PLAYER URLS FROM WEBSITE
def grabMediaDownloadLink(link, start=False, quality=0, headed = False):
	video = None
	default = True

	if not headed:
		options = Options()
		options.add_argument("--headless")	
		driver = webdriver.Firefox(firefox_options=options)
	else:
		driver = webdriver.Firefox()

	driver.set_window_size(1120, 550)

	driver.set_page_load_timeout(5)
	try:
		driver.get(link)
		print("Page loaded..")
	except TimeoutException:
		print("Page timed out, attempting anyway..")

	episodeLinks = driver.find_elements_by_xpath("//div[contains(@class, 'list-episodes')]/a")
	
	videoLinks = [s.get_attribute("href") + "?server=streamango" for s in episodeLinks]
	videos = list()
	for index, item in enumerate(videoLinks):
		print("--EPISODE " + str(index+1) + "--")
		#curPureLink = newRapidVideoFetch(driver, item, quality)
		curPureLink = streamangoFetch(driver, item)
		if curPureLink:
			videos.append(curPureLink)

	driver.quit()
	return videos



# GET ARGS FROM USER COMMAND PROMPT
def parseArguments():
	parser = argparse.ArgumentParser(description="Grabs multiple videos from multiple pages")
	parser.add_argument("-l", "--link", help="URL to fetch from(ep 1)")
	parser.add_argument("-f", "--filename", help="will replace wildcard '$' with episode number")
	parser.add_argument("-s", "--start", type=int, help="starting episode")
	parser.add_argument("-q", "--quality", type=int, help="manually set quality, Ex. 1080, 720, 480")
	parser.add_argument("--skip", action="store_true", help="Skips files that are already in the directory")
	parser.add_argument("--headed", action="store_true", help="Shows visual firefox instance")

	args = parser.parse_args()
	return args

## START EXECUTION

# GET ARGS
args = parseArguments()

# GET ALL DOWNLOAD LINKS
print("---RAW DOWNLOAD---")
videos = grabMediaDownloadLink(args.link, args.start, args.quality, args.headed)

if not videos:
	print("Metadata incomplete..\n")
else:
	print("Metadata fetch completed.\n")

for index, vidLink in enumerate(videos):
	num = index+1	
	curName = args.filename.replace('$', str(num) + ".mp4")

	if args.start and int(num) < args.start:
		continue

	if args.skip and os.path.exists(curName):
		print(curName + " exists, skipping")
		continue

	if args.filename:
		print("Downloading " + args.filename.replace('$', str(num)) + "..")
		Downloader.download(vidLink, args.filename.replace('$', str(num) + ".mp4"))
	else:
		print("Downloading " + str(num) + ".mp4..")
		Downloader.download(vidLink, str(num) + ".mp4")

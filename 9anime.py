#!/bin/env python

#||||||||||||||||||||||||||||||||||||||||||||
# File: 9anime.py
# Author: Shane "SajeOne" Brown
# Desc: 9anime.to ripper
# Date: 29/11/18
#||||||||||||||||||||||||||||||||||||||||||||

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By 
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException
from urllib.request import Request, urlopen
from urllib import error
from bs4 import BeautifulSoup as BS

# LOCAL
from Downloader import Downloader 
from ThreadedDownloader import ThreadedDownloader
import requests
import sys 
import subprocess
import argparse
import json
import os.path

# GLOBALS
g_host = ""

# Selenium custom EC wait
class video_src_loaded(object):
	def __init__(self, locator, attribute):
		self.locator = locator
		self.attribute = attribute

	def __call__(self, driver):
		try:
			element_attribute = EC._find_element(driver, self.locator).get_attribute(self.attribute)
			if element_attribute:
				return True
		except StaleReferenceException:
			pass
		
		return False

def mp4uploadFromFrame(driver, vidLink):
	print("inside mp4upload")
	print(vidLink)

	try:
		driver.get(vidLink)
		print("Page loaded..")
	except TimeoutException:
		print("Page timed out, attempting anyway..")

	frame = None

	try:
		placeholder = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[contains(@class, "cover")]')))
		while True:
			print("Cover found")
			try:
				placeholder.click()
			except:
				print("something is over the cover")
			print("Cover clicked, waiting for iframe..")
			try:
				driver.find_element_by_xpath('//div[contains(@class, "cover")]')
			except NoSuchElementException:
				break
		frame = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//iframe[contains(@src, "mp4upload")]')))
		print("Found mp4upload frame")
	except TimeoutException:
		print("Failed to find frame..")

	if not frame:
		return False

	driver.switch_to.frame(frame)
	
	vTag = None
	try:
		vTag = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'vid_html5_api')))
		WebDriverWait(driver, 10).until(video_src_loaded((By.ID, 'vid_html5_api'), "src"))
		url = vTag.get_attribute("src")
		print("SHOWING REAL URL: " + url)
	except TimeoutException:
		print("Could not find video tag in iframe")

	return vTag

	
def rapidVideoFromFrame(driver, vidLink, quality=0):
	print("--RAPID FETCH--")
	mcloud = False
	try:
		driver.get(vidLink)
		print("Page loaded..")
	except TimeoutException:
		print("Page timed out, attempting anyway..")

	frame = None
	
	try:
		placeholder = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[contains(@class, "cover")]')))
		if placeholder:
			print("Cover found")
			placeholder.click()
			print("Cover clicked, waiting for iframe..")
		frame = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//iframe[contains(@src, "rapidvideo")]')))
		print("Grabbed rapid")
	except TimeoutException:
		try:
			frame = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//iframe[contains(@src, "mcloud")]')))
			print("Grabbed mcloud")
			mcloud = True
		except TimeoutException:
			print("Failed to find frame")

		


	if not frame:
		return False

	# Use new fancy frame switch thingamabooob
	driver.switch_to.frame(frame)

	
	try:
		#captcha = driver.find_element_by_xpath('//input[contains(@id, "captcha_code")]')
		captcha = WebDriverWait(driver, 1).until(EC.presence_of_element_located((By.XPATH, '//input[contains(@id, "captcha_code")]')))
		if captcha != None:
			input("Captcha found, fill out form and hit enter")
	except (NoSuchElementException, TimeoutException):
		print("No captcha found")

	vTag = None
	for i in range(0, 3):
		try:
			vTag = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//video')))
			break
		except TimeoutException:
			print("vTag timeout :(")
	return vTag


def classicVideoRip(driver, vidLink):
	print("--CLASSIC FETCH--")
	try:
		driver.get(vidLink)
		print("Page loaded..")
	except TimeoutException:
		print("Page timed out, attempting anyway..")
	try:
		realVid = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//video')))
	except TimeoutException:
		return False

	return realVid


def grabMediaDownloadLink(link, jsonVideos=False, start=False, quality=0, kill=False, headed = False):
	video = None


	if not headed:
		options = Options()
		options.add_argument("--headless")	
		driver = webdriver.Firefox(firefox_options=options)
	else:
		driver = webdriver.Firefox()

	driver.set_window_size(1120, 550)

	driver.set_page_load_timeout(20)
	try:
		driver.get(link)
		print("Page loaded..")
	except TimeoutException:
		print("Page timed out, attempting anyway..")

	
	listGroup = driver.find_elements_by_xpath("//ul[contains(@class, 'episodes') and not(contains(@class, 'hidden'))]")
	p1Div = listGroup[0]
	linkTags = p1Div.find_elements_by_xpath("li/a")
	print("Test num: " + linkTags[0].text)
	print("Num A tags: " + str(len(linkTags)))
	
	pageLinks = [s.get_attribute("href") for s in linkTags]
	videoNums = [g.get_attribute("textContent") for g in linkTags]
	print(pageLinks)
	print(videoNums)
	videos = dict()

	for index, num in enumerate(videoNums):
		videos[num] = pageLinks[index]

	print("Videos: ")
	print(videos)

	print("Episode pages found, finding videos")
	videoLinks = None
	for num, vidLink in videos.items():
		print("Cur Link: " + vidLink)
		print("start:")
		print(start)
		print("num: " + num)
		print(type(num))
		if start and int(num) < start:
			continue
		if jsonVideos and jsonVideos[num] != "":
			continue

		print("Fetching video link for ep. " + str(num))

		realVid = False
		count = 0
		while realVid == False and count <= 5:
			if g_host == 'mp4upload':
				realVid = mp4uploadFromFrame(driver, vidLink)
			else:
				realVid = rapidVideoFromFrame(driver, vidLink, quality)

			if not realVid:
				count += 1
				print("Attempt " + str(count) + " Failed")
				print("Video load timed out, retrying")

		if not realVid:
			print("Abandoning ep. " + str(num))
			continue

		print("Video source: ")
		print(realVid.get_attribute("src"))

		if jsonVideos:
			jsonVideos[num] = realVid.get_attribute("src")
			if kill and not jsonVideos[num]:
				print("Kill enabled, fetch failed")
				sys.exit(1)
		else:
			videos[num] = realVid.get_attribute("src")
			if kill and not videos[num]:
				print("Kill enabled, fetch failed")
				sys.exit(1)

		
	driver.quit()
	return videos



def parseArguments():
	parser = argparse.ArgumentParser(description="Grabs multiple videos from multiple pages")
	parser.add_argument("-l", "--link", help="URL to fetch from(ep 1)")
	parser.add_argument("-f", "--filename", help="will replace wildcard '$' with episode number")
	parser.add_argument("-j", "--jsonfile", help="json file for show")
	parser.add_argument("-s", "--start", type=int, help="starting episode")
	parser.add_argument("-q", "--quality", type=int, help="manually set quality, Ex. 1080, 720, 480")
	parser.add_argument("-k", "--kill", action="store_true", help="kills fetch if link failed to be found")
	parser.add_argument("-d", "--dryrun", action="store_true", help="Just fetches urls to store in json file")
	parser.add_argument("--skip", action="store_true", help="Skips files that are already in the directory")
	parser.add_argument("--headed", action="store_true", help="Shows visual firefox instance")
	parser.add_argument("--single-threaded", action="store_true", help="uses single threaded downloader")
	parser.add_argument("--host", help="force certain host (rapidvideo, mp4upload..)")

	args = parser.parse_args()
	return args


args = parseArguments()

if args.host:
	g_host = args.host

if args.link and args.jsonfile:
	print("---PATCH DOWNLOAD---")
	jsonVideos = False
	with open(args.jsonfile, 'r') as fileHandle:
		jsonVideos = json.load(fileHandle)	
	videos = grabMediaDownloadLink(args.link, jsonVideos, args.start, args.quality, args.kill, args.headed)
	
elif not args.jsonfile:
	print("---RAW DOWNLOAD---")
	videos = grabMediaDownloadLink(args.link, False, args.start, args.quality, args.kill, args.headed)
	with open(args.filename.replace('$', "all.json"), 'w') as fileHandle:
		json.dump(videos, fileHandle)
else:
	print("---JSON DOWNLOAD---")
	videos = False
	with open(args.jsonfile, 'r') as fileHandle:
		videos = json.load(fileHandle)

if not videos:
	print("Metadata incomplete..\n")
else:
	print("Metadata fetch completed.\n")

newlist = list()
if not args.link or not args.jsonfile:
	for num, vidLink in videos.items():
		curName = args.filename.replace('$', str(num) + ".mp4")
		obj = dict()
		obj['name'] = curName
		obj['url'] = vidLink
		newlist.append(obj)
		
		if args.single_threaded and not args.dryrun:
			if args.start and int(num) < args.start:
				continue

			if args.skip and os.path.exists(curName):
				print(curName + " exists, skipping")
				continue

			if args.filename:
				print("Downloading " + args.filename.replace('$', str(num)) + "..")
				Downloader.download(vidLink, args.filename.replace('$', str(num) + ".mp4"))
			else:
				print("Downloading " + str(num) + ".mp4..")
				Downloader.download(vidLink, str(num) + ".mp4")
	if not args.single_threaded and not args.dryrun:
		print(newlist)
		dl = ThreadedDownloader(newlist)
		dl.run()
